# Demo Profile Exporter project.
Import .json or .xml files with 'profile' info into MongoDB collection.

## How to run
### Console application:

    mvn clean verify
    java -jar target/pdp-profile-exporter-1.0-SNAPSHOT.jar import input_data/profiles.json
    java -jar target/pdp-profile-exporter-1.0-SNAPSHOT.jar showAll

### Web application:

    mvn clean verify
    mvn cargo:run

#### Urls for checks:

MongoDB REST Controller:  

    http://localhost:8080/api/mongodb/profiles
    http://localhost:8080/api/mongodb/profiles?dateFormat=yyyy-MM-dd

Elasticsearch REST Controller:

    POST http://localhost:8080/api/es/profiles/100
    {
        "profileName": "kireno2",
        "profileType": "PUBLIC",
        "registerDate": 1612375666508,
        "description": "Suspendisse potenti."
    }

    GET http://localhost:8080/api/es/profiles/100

    PUT http://localhost:8080/api/es/profiles/100
    {
        "profileType": "PROTECTED"
    }

    DELETE http://localhost:8080/api/es/profiles/100

Elasticsearch search endpoints:

- Aggregate by profile types within date range:  
    http://localhost:8080/api/es/profiles/types?dateFrom=2021-02-01&dateTo=2021-04-01

- Find profiles by profile name (at least 3 symbols, with 1 mistake, case-insensitive):  
    http://localhost:8080/api/es/profiles/findProfiles?name=Mlik
  
- Find profiles in date range with sorting and excluding profiles types:
  http://localhost:8080/api/es/profiles?dateFrom=2021-02-01&dateTo=2021-05-01&excludedTypes=PRIVATE,PUBLIC&sortBy=registerDate&order=DESC


## Elasticsearch setup
To prepare and populate Elasticsearch `profiles` index run queries 
`01_profiles_setting_and_mapping.txt` and `02_populate_profiles_bulk.txt` 
from `elasticsearch` folder. Other files contains queries for testing.
