package com.romash;

import com.romash.util.MongoDBHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


public class MongoDBBaseIntegrationTestClass extends BaseIntegrationTestClass{

    @Autowired
    private MongoDBHelper mongoDBHelper;

    @BeforeMethod
    public void beforeMethod() {
        mongoDBHelper.prepare();
    }

    @AfterMethod
    public void afterMethod() {
        mongoDBHelper.cleanup();
    }

}
