package com.romash.util;

import org.apache.http.HttpHost;
import org.codelibs.elasticsearch.runner.ElasticsearchClusterRunner;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class ElasticsearchHelper {

    @Value("${elasticsearch.host}")
    private String host;

    @Value("${elasticsearch.port}")
    private int port;

    @Value("${elasticsearch.scheme}")
    private String scheme;

    @Value("${elasticsearch.index}")
    private String PROFILES_INDEX;

    private final Path PROFILES_PATH = Paths.get("src/integration-test/test/resources/input/profiles_bulk.txt");
    private final Path INDEX_SETTINGS = Paths.get("src/integration-test/test/resources/input/profiles_settings_index.json");
    private byte[] profilesBulk;
    private String indexSetting;

    private RestHighLevelClient client;

    ElasticsearchClusterRunner runner;

    @PostConstruct
    public void init() throws IOException {
        client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(host, port, scheme)));
        profilesBulk = Files.readAllBytes(PROFILES_PATH);
        indexSetting = new String(Files.readAllBytes(INDEX_SETTINGS));
    }

    public void prepareCluster() {
        // create runner instance
        runner = new ElasticsearchClusterRunner();
        runner.build(ElasticsearchClusterRunner.newConfigs()
                .clusterName("elasticsearch-test")
                .baseHttpPort(8200)
                .numOfNode(1)
                .basePath("target/temp_cluster")
                .disableESLogger());
        runner.ensureYellow();
    }

    public void cleanupCluster() throws IOException {
        runner.close();
        runner.clean();
    }

    public void prepareIndex() throws IOException {
        if (client.indices().exists(new GetIndexRequest(PROFILES_INDEX), RequestOptions.DEFAULT)) {
            client.indices().delete(new DeleteIndexRequest(PROFILES_INDEX), RequestOptions.DEFAULT);
        }

        // create index with settings and mappings
        CreateIndexRequest createIndexRequest = new CreateIndexRequest(PROFILES_INDEX);
        createIndexRequest.source(indexSetting, XContentType.JSON);
        client.indices().create(createIndexRequest, RequestOptions.DEFAULT);

        // populate index with test data
        BulkRequest bulkRequest = new BulkRequest(PROFILES_INDEX);
        bulkRequest.add(profilesBulk, 0, profilesBulk.length, XContentType.JSON);
        client.bulk(bulkRequest, RequestOptions.DEFAULT);

        // refresh index
        client.indices().refresh(new RefreshRequest(), RequestOptions.DEFAULT);
    }
}
