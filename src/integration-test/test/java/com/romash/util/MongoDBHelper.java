package com.romash.util;

import com.mongodb.client.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MongoDBHelper {

    @Autowired
    private MongoClient mongoClient;

    @Value("${mongodb.database}")
    private String database;

    @Value("${mongodb.collection}")
    private String collection;

    public void prepare() {
        System.out.println("from MongoDBHelper");
        mongoClient.getDatabase(database).createCollection(collection);
    }

    public void cleanup() {
        mongoClient.getDatabase(database).drop();
    }
}
