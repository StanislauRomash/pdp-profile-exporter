package com.romash.util;

import com.romash.domain.Profile;
import com.romash.domain.ProfileType;
import com.romash.repository.DBFields;
import org.bson.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static List<Profile> getProfiles() {
        List<Profile> profiles = new ArrayList<>();

        Profile profile = new Profile();
        profile.setProfileName("mlilley0");
        profile.setRegisterDate(LocalDate.parse("2021-02-09"));
        profile.setProfileType(ProfileType.PUBLIC);
        profile.setExportDateTime(LocalDateTime.now());
        profiles.add(profile);

        profile = new Profile();
        profile.setProfileName("amachen3");
        profile.setRegisterDate(LocalDate.parse("2021-02-04"));
        profile.setProfileType(ProfileType.PRIVATE);
        profile.setExportDateTime(LocalDateTime.now());
        profile.setAdditionalField("errorCode", "397");
        profile.setAdditionalField("description",
                "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.");
        profiles.add(profile);
        return profiles;
    }

    public static List<Document> getDocuments() {
        List<Document> documents = new ArrayList<>();

        Document document = new Document(DBFields.PROFILE_NAME, "mlilley0")
                .append(DBFields.REGISTER_DATE, "2021-02-09")
                .append(DBFields.PROFILE_TYPE, "PUBLIC")
                .append(DBFields.EXPORT_DATE_TIME, System.currentTimeMillis());
        documents.add(document);

        document = new Document(DBFields.PROFILE_NAME, "amachen3")
                .append(DBFields.REGISTER_DATE, "2021-02-04")
                .append(DBFields.PROFILE_TYPE, "PRIVATE")
                .append(DBFields.EXPORT_DATE_TIME, System.currentTimeMillis())
                .append("errorCode", "397")
                .append("description",
                        "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.");
        documents.add(document);
        return documents;
    }
}
