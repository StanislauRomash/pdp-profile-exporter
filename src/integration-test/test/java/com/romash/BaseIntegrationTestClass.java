package com.romash;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@ContextConfiguration({"/profile-exporter-common-services-context-test.xml"})
@TestPropertySource(locations = "classpath:application-test.properties")
public abstract class BaseIntegrationTestClass extends AbstractTestNGSpringContextTests {
}
