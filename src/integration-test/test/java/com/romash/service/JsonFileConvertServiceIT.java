package com.romash.service;

import com.romash.BaseIntegrationTestClass;
import com.romash.domain.Profile;
import com.romash.service.exception.ConvertionException;
import com.romash.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class JsonFileConvertServiceIT extends BaseIntegrationTestClass {

    @Autowired
    private JsonFileConvertService service;

    private final String CORRECT_FILE = "src/integration-test/test/resources/input/correct.json";
    private final String ABSENT_FILE = "absent.xml";


    @Test
    public void testCorrectConvert() throws ConvertionException {
        // given
        List<Profile> expectedProfiles = Utils.getProfiles();
        LocalDateTime currentTime = LocalDateTime.now();

        // when
        List<Profile> actualProfiles = service.convert(CORRECT_FILE);

        // then
        assertEquals(actualProfiles, expectedProfiles);
        for (Profile profile : actualProfiles) {
            long delay = Duration.between(profile.getExportDateTime(), currentTime)
                    .toMillis();
            System.out.println("profile.getExportDateTime(): " + profile.getExportDateTime());
            System.out.println("LocalDateTime.now(): " + currentTime);
            System.out.println("delay: " + delay);
            // something brings delay about 20 seconds during build the artifact
            assertTrue(delay < 60000);
        }
    }

    @Test(expectedExceptions = ConvertionException.class)
    public void testAbsentFile() throws ConvertionException {
        // when
        service.convert(ABSENT_FILE);
    }
}
