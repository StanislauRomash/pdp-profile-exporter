package com.romash.service;

import com.romash.BaseIntegrationTestClass;
import com.romash.domain.Profile;
import com.romash.service.exception.ConvertionException;
import com.romash.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class XmlFileConvertServiceIT extends BaseIntegrationTestClass {

    @Autowired
    XmlFileConvertService service;

    private final String CORRECT_FILE = "src/integration-test/test/resources/input/correct.xml";
    private final String ABSENT_FILE = "absent.xml";

    @Test
    public void testCorrectConvert() throws ConvertionException {
        // given
        List<Profile> expectedProfiles = Utils.getProfiles();

        // when
        List<Profile> actualProfiles = service.convert(CORRECT_FILE);

        // then
        assertEquals(actualProfiles, expectedProfiles);
        for (Profile profile : actualProfiles) {
            long delay = Duration.between(profile.getExportDateTime(), LocalDateTime.now())
                    .toMillis();
            assertTrue(delay < 1000);
        }
    }

    @Test(expectedExceptions = ConvertionException.class)
    public void testAbsentFile() throws ConvertionException {
        // when
        service.convert(ABSENT_FILE);
    }
}
