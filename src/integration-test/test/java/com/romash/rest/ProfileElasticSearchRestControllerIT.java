package com.romash.rest;

import com.google.common.collect.Ordering;
import com.jayway.jsonpath.JsonPath;
import com.romash.BaseWebIntegrationTestClass;
import com.romash.util.ElasticsearchHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProfileElasticSearchRestControllerIT extends BaseWebIntegrationTestClass {

    private final String BASE_URL = "/api/es/profiles";

    @Autowired
    private ElasticsearchHelper elasticsearchHelper;

    @BeforeClass
    public void prepareCluster() {
        elasticsearchHelper.prepareCluster();
    }

    @AfterClass
    public void shutDownCluster() throws IOException {
        elasticsearchHelper.cleanupCluster();
    }

    @BeforeMethod
    public void prepare() throws IOException {
        elasticsearchHelper.prepareIndex();
    }

    // Expected: { "PUBLIC": 10, "PROTECTED": 5, "PRIVATE": 5 }
    @Test
    public void testGetTypesFromAllRecords() throws Exception {
        mockMvc
                .perform(get(BASE_URL + "/types"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.PUBLIC", is(10)))
                .andExpect(jsonPath("$.PROTECTED", is(5)))
                .andExpect(jsonPath("$.PRIVATE", is(5)))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andReturn();
    }

    // Expected: { "PUBLIC": 5, "PRIVATE": 4 }
    @Test
    public void testGetTypesFromRecordsWithinDateRange() throws Exception {
        mockMvc
                .perform(get(BASE_URL + "/types")
                        .param("dateFrom", "2021/02/01")
                        .param("dateTo", "2021/04/01")
                        .param("dateFormat", "yyyy/MM/dd"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.PUBLIC", is(5)))
                .andExpect(jsonPath("$.PRIVATE", is(4)))
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andReturn();
    }

    @Test
    public void testFindOneProfile() throws Exception {
        mockMvc
                .perform(get(BASE_URL + "/findProfiles")
                        .param("name", "Bli"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(2)))
                .andExpect(jsonPath("$.[0].profileName", is("mlilley0")))
                .andExpect(jsonPath("$.[1].profileName", is("beinchcombe2")))
                .andReturn();
    }

    @Test
    public void testFindAllProfile() throws Exception {
        mockMvc
                .perform(get(BASE_URL)
                        .param("size", "100"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(20)))
                .andReturn();
    }

    // http://localhost:8080/api/es/profiles?dateFrom=2021/02/01&dateTo=2021/05/01&dateFormat=yyyy/MM/dd&excludedTypes=PRIVATE,PUBLIC
    @Test
    public void testFindProfilesInRangeAndWithExlusions() throws Exception {
        mockMvc
                .perform(get(BASE_URL)
                        .param("dateFrom", "2021/02/01")
                        .param("dateTo", "2021/05/01")
                        .param("dateFormat", "yyyy/MM/dd")
                        .param("excludedTypes", "PRIVATE,PUBLIC")
                        .param("size", "100"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(4)))
                .andExpect(jsonPath("$..profileType", everyItem(is("PROTECTED"))))
                .andExpect(jsonPath("$..profileName", containsInAnyOrder(
                                "wbeddard5", "cisenor9", "amachen3", "jgaize1")))
                .andReturn();
    }

    // http://localhost:8080/api/es/profiles?sortBy=registerDate&order=DESC
    @Test
    public void testFindProfilesSorted() throws Exception {
        String content = mockMvc
                .perform(get(BASE_URL)
                        .param("sortBy", "registerDate")
                        .param("order", "DESC")
                        .param("size", "100"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(20)))
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<String> dates = JsonPath.parse(content).read("$..registerDate");
        Assert.assertTrue(Ordering.from(Comparator.<String>reverseOrder())
                .isOrdered(dates));
    }

    // http://localhost:8080/api/es/profiles/100
    @Test
    public void testProfileNotFound() throws Exception {
        mockMvc
                .perform(get(BASE_URL + "/100"))
                .andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    // http://localhost:8080/api/es/profiles/100
    @Test
    public void testUpdateInexistentProfile() throws Exception {
        mockMvc
                .perform(put(BASE_URL + "/100")
                        .content("{\"profileType\": \"PROTECTED\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(500))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
    }

}
