package com.romash;

import com.romash.domain.Profile;
import com.romash.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.Test;

import java.util.Collection;

import static org.testng.Assert.assertEquals;

@ContextConfiguration({"/profile-exporter-console-root-context-test.xml"})
public class ProfileConsoleControllerIT extends MongoDBBaseIntegrationTestClass {

    private final String CORRECT_JSON_FILE = "src/integration-test/test/resources/input/correct.json";

    @Autowired
    ProfileConsoleController controller;

    @Test
    public void testImportFileJson() {
        // given
        Collection<Profile> expectedProfiles = Utils.getProfiles();

        // when
        controller.importFile(CORRECT_JSON_FILE);

        // then
        Collection<Profile> actualProfiles = controller.showAll();
        assertEquals(actualProfiles, expectedProfiles);
    }

}
