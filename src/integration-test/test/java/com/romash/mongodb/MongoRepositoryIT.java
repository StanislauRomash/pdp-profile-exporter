package com.romash.mongodb;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Projections;
import com.romash.MongoDBBaseIntegrationTestClass;
import com.romash.repository.MongoRepository;
import com.romash.util.Utils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class MongoRepositoryIT extends MongoDBBaseIntegrationTestClass {

    @Autowired
    private MongoRepository repository;

    @Test
    public void testSaveAndFindAll(){
        // given
        List<Document> expectedDocuments = Utils.getDocuments();
        Bson projection = Projections.excludeId();

        // when
        repository.saveAll(expectedDocuments);

        // then
        // documents from repository should be returned without "_id" field.
        expectedDocuments.forEach(document -> document.remove("_id"));
        FindIterable<Document> documentFindIterable = repository.findAll();
        List<Document> actualDocumetns = documentFindIterable
                .projection(projection)
                .into(new ArrayList<>());
        assertEquals(actualDocumetns, expectedDocuments);
    }

}
