package com.romash.mongodb;

import com.romash.MongoDBBaseIntegrationTestClass;
import com.romash.domain.Profile;
import com.romash.service.MongoDBRepositoryService;
import com.romash.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class MongoDBRepositoryServiceIT extends MongoDBBaseIntegrationTestClass {

    @Autowired
    private MongoDBRepositoryService service;

    @Test
    public void testSaveAndFindAll() {
        // given
        List<Profile> expectedProfiles = Utils.getProfiles();

        // when
        service.saveAll(expectedProfiles);

        // then
        List<Profile> actualProfiles = service.findAll();
        assertEquals(actualProfiles, expectedProfiles);
    }
}
