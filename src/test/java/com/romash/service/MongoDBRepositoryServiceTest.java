package com.romash.service;

import com.mongodb.client.FindIterable;
import com.romash.BaseTestClass;
import com.romash.domain.Profile;
import com.romash.repository.Repository;
import com.romash.util.ProfileConverter;
import org.bson.Document;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class MongoDBRepositoryServiceTest extends BaseTestClass {

    @Mock
    private  Repository mongoRepository;

    @Mock
    private  ProfileConverter converter;

    @Mock
    private List<Profile> profiles;

    @Mock
    private List<Document> documents;

    @Mock
    private FindIterable<Document> documentIterable;

    private MongoDBRepositoryService service;

    @BeforeMethod
    public void initMethod() {
        service = new MongoDBRepositoryService(mongoRepository, converter);
    }

    @Test
    public void testSaveAll() {
        // given
        when(converter.convertToDocuments(profiles)).thenReturn(documents);

        // when
        service.saveAll(profiles);

        // then
        verify(converter).convertToDocuments(profiles);
        verify(mongoRepository).saveAll(documents);
        verifyNoMoreInteractions(mongoRepository, converter);
    }

    @Test
    public void testFindAll() {
        // given
        when(mongoRepository.findAll()).thenReturn(documentIterable);
        when(documentIterable.into(any(List.class))).thenReturn(documents);
        when(converter.convertToProfiles(documents)).thenReturn(profiles);

        // when
        List<Profile> actualProfiles = service.findAll();

        // then
        assertEquals(actualProfiles, profiles);
        verify(mongoRepository).findAll();
        verify(documentIterable).into(any(List.class));
        verify(converter).convertToProfiles(documents);
        verifyNoMoreInteractions(mongoRepository, documentIterable, converter);
    }
}
