package com.romash.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.romash.BaseTestClass;
import com.romash.domain.Profile;
import com.romash.service.exception.ConvertionException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class JsonFileConvertServiceTest extends BaseTestClass {

    @Mock
    private ObjectMapper objectMapper;

    private JsonFileConvertService service;

    private final String FILENAME = "filename";

    @BeforeMethod
    public void initMethod() {
        service = new JsonFileConvertService(objectMapper);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testCorrectInput() throws IOException, ConvertionException {
        // given
        ArrayList<Profile> expected = mock(ArrayList.class);
        when(objectMapper.readValue(any(File.class), any(TypeReference.class)))
                .thenReturn(expected);

        // when
        List<Profile> result = service.convert(FILENAME);

        // then
        assertEquals(result, expected);
        verify(objectMapper).readValue(any(File.class), any(TypeReference.class));
        verifyNoMoreInteractions(objectMapper);
    }

    @DataProvider
    public Object[][] incorrectInputs() {
        return new Object[][] {
                {new IOException()},
                {new NumberFormatException()}
        };
    }

    @Test(dataProvider = "incorrectInputs",
            expectedExceptions = {ConvertionException.class})
    @SuppressWarnings("unchecked")
    public void testIncorrectInput(Throwable exception) throws ConvertionException, IOException {
        // given
        when(objectMapper.readValue(any(File.class), any(TypeReference.class)))
                .thenThrow(exception);

        // when
        service.convert(FILENAME);

        // then
        verify(objectMapper).readValue(any(File.class), any(TypeReference.class));
        verifyNoMoreInteractions(objectMapper);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testEmptyConvertedOutput() throws IOException, ConvertionException {
        // given
        when(objectMapper.readValue(any(File.class), any(TypeReference.class)))
                .thenReturn(null);

        // when
        List<Profile> result = service.convert(FILENAME);

        // then
        assertTrue(result.isEmpty());
        verify(objectMapper).readValue(any(File.class), any(TypeReference.class));
        verifyNoMoreInteractions(objectMapper);
    }

    @AfterMethod
    public void afterMethod() {
        Mockito.reset(objectMapper);
    }
}
