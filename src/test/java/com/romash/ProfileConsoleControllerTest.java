package com.romash;

import com.romash.domain.Profile;
import com.romash.service.FileConvertService;
import com.romash.service.RepositoryService;
import com.romash.service.exception.ConvertionException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class ProfileConsoleControllerTest extends BaseTestClass {

    @Mock
    private FileConvertService jsonFileConvertService;

    @Mock
    private FileConvertService xmlFileConvertService;

    @Mock
    private RepositoryService repositoryService;

    @Mock
    private List<Profile> profiles;

    private ProfileConsoleController controller;

    @BeforeMethod
    public void initMethod() {
        controller = new ProfileConsoleController(jsonFileConvertService, xmlFileConvertService, repositoryService);
    }

    @Test
    public void testImportFileJson() throws ConvertionException {
        // given
        String filename = "file.json";
        when(jsonFileConvertService.convert(filename)).thenReturn(profiles);
        // when
        controller.importFile(filename);

        // then
        verify(jsonFileConvertService).convert(filename);
        verify(repositoryService).saveAll(profiles);
        verifyNoMoreInteractions(jsonFileConvertService, xmlFileConvertService, repositoryService);
    }

    @Test
    public void testImportFileXml() throws ConvertionException {
        // given
        String filename = "file.xml";
        when(xmlFileConvertService.convert(filename)).thenReturn(profiles);
        // when
        controller.importFile(filename);

        // then
        verify(xmlFileConvertService).convert(filename);
        verify(repositoryService).saveAll(profiles);
        verifyNoMoreInteractions(jsonFileConvertService, xmlFileConvertService, repositoryService);
    }

    @Test
    public void testImportIncorrectFileFormat() {
        // given
        String filename = "file.incorrect";

        // when
        controller.importFile(filename);

        // then
        verifyNoMoreInteractions(jsonFileConvertService, xmlFileConvertService, repositoryService);
    }

    @Test()
    public void testImportFileCauseException() throws ConvertionException {
        // given
        String filename = "file.json";
        when(jsonFileConvertService.convert(filename)).thenThrow(ConvertionException.class);
        // when
        controller.importFile(filename);

        // then
        verify(jsonFileConvertService).convert(filename);
        verifyNoMoreInteractions(jsonFileConvertService, xmlFileConvertService, repositoryService);
    }

    @Test
    public void testShowAll() {
        // given
        when(repositoryService.findAll()).thenReturn(profiles);

        // when
        controller.showAll();

        // then
        verify(repositoryService).findAll();
        verifyNoMoreInteractions(jsonFileConvertService, xmlFileConvertService, repositoryService);
    }

    @AfterMethod
    public void afterMethod() {
        Mockito.reset(jsonFileConvertService, xmlFileConvertService, repositoryService, profiles);
    }

}
