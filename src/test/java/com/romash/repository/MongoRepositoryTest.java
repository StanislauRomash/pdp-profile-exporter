package com.romash.repository;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.romash.BaseTestClass;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class MongoRepositoryTest extends BaseTestClass {

    @Mock
    private MongoClient mongoClient;

    @Mock
    private MongoDatabase mongoDatabase;

    @Mock
    private MongoCollection<Document> collection;

    private MongoRepository repository;

    private String MONGO_DB = "database";
    private String MONGO_COLLECTION = "collection";

    @BeforeMethod
    public void initMethod() {
        when(mongoClient.getDatabase(MONGO_DB)).thenReturn(mongoDatabase);
        when(mongoDatabase.getCollection(MONGO_COLLECTION)).thenReturn(collection);
        repository = new MongoRepository(mongoClient, MONGO_DB, MONGO_COLLECTION);
    }

    @Test
    public void testSaveAll() {
        // given
        List<Document> documents = mock(List.class);

        // when
        repository.saveAll(documents);

        // then
        verify(collection).insertMany(documents);
        verifyNoMoreInteractions(collection);
    }

    @Test
    public void testFindAll() {
        // given
        FindIterable<Document> found = mock(FindIterable.class);
        FindIterable<Document> expected = mock(FindIterable.class);
        when(collection.find()).thenReturn(found);
        when(found.projection(any(Bson.class))).thenReturn(expected);

        // when
        FindIterable<Document> result = repository.findAll();

        // then
        assertEquals(result, expected);
        verify(collection).find();
        verify(found).projection(any(Bson.class));
        verifyNoMoreInteractions(collection, found);
    }
}
