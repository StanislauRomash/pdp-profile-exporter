package com.romash.util;

import com.romash.BaseTestClass;
import com.romash.domain.Profile;
import org.bson.Document;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ProfileConverterTest extends BaseTestClass {

    private ProfileConverter converter;

    @BeforeMethod
    public void initMethod() {
        converter = new ProfileConverter();
    }

    @Test
    public void testConvertProfileToDocument() {
        // given
        Profile profile = getProfile();
        Document expected = getDocument();

        // when
        Document result = converter.convertToDocument(profile);

        // then
        assertEquals(result, expected);
    }

    @Test
    public void testConvertDocumentToProfile() {
        // given
        Document document = getDocument();
        Profile expected = getProfile();

        // when
        Profile result = converter.convertToProfile(document);

        // then
        assertEquals(result, expected);
    }

}
