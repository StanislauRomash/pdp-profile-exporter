package com.romash.util;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DateTransformerTest {

    private String correctDate = "1613746215084";

    private DateTransformer dateTransformer;

    @DataProvider
    public Object[][] correctDates() {
        return new Object[][] {
                {DateFormats.DMY, correctDate, "19/02/2021"},
                {DateFormats.MDY, correctDate, "02/19/2021"},
                {DateFormats.YMD, correctDate, "2021-02-19"}
        };
    }

    @Test(dataProvider = "correctDates")
    public void testCorrectDate(DateFormats dateFormat, String dateMillis, String expected) {
        // given
        DateTransformer dateTransformer = new DateTransformer(dateFormat);

        // when
        String result = dateTransformer.transformMillisToStringDate(dateMillis);

        // then
        assertEquals(result, expected);
    }

    @DataProvider
    public Object[][] incorrectDates() {
        return new Object[][] {
                {DateFormats.DMY, "incorrect"},
                {DateFormats.MDY, ""},
                {DateFormats.YMD, null},
                {DateFormats.DMY, Long.MAX_VALUE + "0"}, // too big value
                {DateFormats.DMY, Long.MIN_VALUE + "0"} // too small value
        };
    }

    @Test(dataProvider = "incorrectDates",
            expectedExceptions = {NumberFormatException.class})
    public void testIncorrectDates(DateFormats dateFormat, String dateMillis) {
        // given
        DateTransformer dateTransformer = new DateTransformer(dateFormat);

        // when
        String result = dateTransformer.transformMillisToStringDate(dateMillis);
    }
}
