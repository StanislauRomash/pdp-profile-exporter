package com.romash;

import com.romash.domain.Profile;
import com.romash.domain.ProfileType;
import org.bson.Document;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public abstract class BaseTestClass {

    public static final String PROFILE_NAME = "profileName";
    public static final String REGISTER_DATE = "registerDate";
    public static final String PROFILE_TYPE = "profileType";
    public static final String ERROR_CODE = "errorCode";
    public static final String DESCRIPTION = "description";
    public static final String EXPORT_DATE_TIME = "exportDateTime";

    public static final String PROFILE_NAME_VALUE = "amachen3";
    public static final String REGISTER_DATE_VALUE = "1612398472440";
    public static final ProfileType PROFILE_TYPE_VALUE = ProfileType.PUBLIC;
    public static final long REGISTER_DATE_MILLIS_VALUE = 1612396800000L; // at start of day
    public static final String REGISTER_DATE_ISO_VALUE = "2021-02-04";
    public static final String ERROR_CODE_VALUE = "397";
    public static final String DESCRIPTION_VALUE = "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.";
    public static final long EXPORT_DATE_TIME_MILLIS_VALUE = 1612698472440L;
    public static final LocalDateTime EXPORT_DATE_TIME_VALUE = Instant.ofEpochMilli(EXPORT_DATE_TIME_MILLIS_VALUE)
            .atOffset(ZoneOffset.UTC).toLocalDateTime();

    private AutoCloseable closeable;

    @BeforeClass
    public void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterClass
    public void releaseMocks() throws Exception {
        closeable.close();
    }

    public Profile getProfile() {
        Profile profile = new Profile();
        profile.setProfileName(PROFILE_NAME_VALUE);
        profile.setRegisterDate(LocalDate.parse(REGISTER_DATE_ISO_VALUE));
        profile.setProfileType(PROFILE_TYPE_VALUE);
        profile.setExportDateTime(EXPORT_DATE_TIME_VALUE);
        profile.setAdditionalField(ERROR_CODE, ERROR_CODE_VALUE);
        profile.setAdditionalField(DESCRIPTION, DESCRIPTION_VALUE);
        return profile;
    }

    public Document getDocument() {
        return new Document(PROFILE_NAME, PROFILE_NAME_VALUE)
                .append(REGISTER_DATE, REGISTER_DATE_MILLIS_VALUE)
                .append(PROFILE_TYPE, PROFILE_TYPE_VALUE.toString())
                .append(EXPORT_DATE_TIME, EXPORT_DATE_TIME_MILLIS_VALUE)
                .append(ERROR_CODE, ERROR_CODE_VALUE)
                .append(DESCRIPTION, DESCRIPTION_VALUE);
    }

}
