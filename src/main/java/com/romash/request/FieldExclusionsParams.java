package com.romash.request;

import java.util.ArrayList;
import java.util.List;

public class FieldExclusionsParams {

    private String exclusionField;
    private List<String> excludedValues;

    public FieldExclusionsParams(String exclusionField, List<String> excludedValues) {
        this.exclusionField = exclusionField;
        this.excludedValues = excludedValues;
    }

    public String getExclusionField() {
        return exclusionField;
    }

    public void setExclusionField(String exclusionField) {
        this.exclusionField = exclusionField;
    }

    public List<String> getExcludedValues() {
        return excludedValues != null ? excludedValues
                                      : new ArrayList<>();
    }

    public void setExcludedValues(List<String> excludedValues) {
        this.excludedValues = excludedValues;
    }
}
