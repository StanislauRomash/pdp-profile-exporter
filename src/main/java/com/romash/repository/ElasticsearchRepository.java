package com.romash.repository;

import com.romash.repository.exception.ProfileNotFoundException;
import org.apache.http.HttpHost;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchResponseSections;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.get.GetResult;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Objects;

@Component
public class ElasticsearchRepository  {

    private static final Logger LOGGER = LoggerFactory.getLogger(
            ElasticsearchRepository.class);

    private final String PROFILES_INDEX;

    private final RestHighLevelClient client;

    public ElasticsearchRepository(
            @Value("${elasticsearch.host}") String host,
            @Value("${elasticsearch.port}") int port,
            @Value("${elasticsearch.scheme}") String scheme,
            @Value("${elasticsearch.index}") String index) {
        client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(host, port, scheme)));
        PROFILES_INDEX = index;
    }

    public void indexDocument(String jsonDocument, String id) throws IOException {
        IndexRequest indexRequest = new IndexRequest(PROFILES_INDEX)
                .id(id)
                .source(jsonDocument, XContentType.JSON);
        IndexResponse indexResponse =  client.index(indexRequest, RequestOptions.DEFAULT);
        LOGGER.info("Document with id={} is indexed at: {}. Response: {}",
                indexResponse.getId(), indexResponse.getLocation(null), indexResponse);
    }

    public String getById(String id) throws IOException, ProfileNotFoundException {
        GetRequest getRequest = new GetRequest(PROFILES_INDEX, id);
        GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
        LOGGER.info("Getting document with id={}  at: {}. Response: {}",
                getResponse.getId(), getResponse.getIndex(), getResponse);
        if (getResponse.isExists()) {
            return getResponse.getSourceAsString();
        } else {
            throw new ProfileNotFoundException(id);
        }
    }

    public void deleteById(String id) throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest(PROFILES_INDEX, id);
        DeleteResponse deleteResponse = client.delete(deleteRequest, RequestOptions.DEFAULT);
        LOGGER.info("Document with id={} is deleted at: {}. Response: {}",
                deleteResponse.getId(), deleteResponse.getLocation(null), deleteResponse);
    }

    public String updateById(String jsonPartialDocument, String id) throws IOException {
        UpdateRequest updateRequest = new UpdateRequest(PROFILES_INDEX, id)
                .doc(jsonPartialDocument, XContentType.JSON)
                .fetchSource(true);
//        updateRequest.docAsUpsert(true);
        UpdateResponse updateResponse = client.update(updateRequest, RequestOptions.DEFAULT);
        LOGGER.info("Updating document with id={}  at: {}. Response: {}",
                updateResponse.getId(), updateResponse.getLocation(null), updateResponse);
        GetResult result = updateResponse.getGetResult();
        if (result.isExists()) {
            return result.sourceAsString();
        } else {
            return null;
        }
    }

    public SearchResponseSections search(SearchSourceBuilder searchSourceBuilder) throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(PROFILES_INDEX);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        return searchResponse.getInternalResponse();
    }

    @PreDestroy
    public void destroy() {
        if (Objects.nonNull(client)) {
            try {
                client.close();
            } catch (IOException ioException) {
                // ignore
                ioException.printStackTrace();
            }
        }
    }
}
