package com.romash.repository;

public class DBFields {

    public static final String PROFILE_NAME = "profileName";
    public static final String REGISTER_DATE = "registerDate";
    public static final String EXPORT_DATE_TIME = "exportDateTime";
    public static final String PROFILE_TYPE = "profileType";

    private DBFields() {
        // private constructor for helper class
    }

}
