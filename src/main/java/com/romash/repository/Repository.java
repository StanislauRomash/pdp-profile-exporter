package com.romash.repository;

import com.mongodb.client.FindIterable;
import org.bson.Document;

import java.util.List;

public interface Repository {

    FindIterable<Document> findAll();

    void saveAll(List<Document> documents);
}
