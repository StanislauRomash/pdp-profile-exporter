package com.romash.repository;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

@org.springframework.stereotype.Repository("mongo")
public class MongoRepository implements Repository{

    private MongoCollection<Document> collection;

    @Autowired
    public MongoRepository(MongoClient mongoClient,
                           @Value("${mongodb.database}") String databaseName,
                           @Value("${mongodb.collection}") String collectionName) {
        collection = mongoClient.getDatabase(databaseName)
                                .getCollection(collectionName);
    }

    @Override
    public FindIterable<Document> findAll() {
        Bson projection = Projections.excludeId();
        return collection.find().projection(projection);
    }

    @Override
    public void saveAll(List<Document> documents) {
        collection.insertMany(documents);
    }
}
