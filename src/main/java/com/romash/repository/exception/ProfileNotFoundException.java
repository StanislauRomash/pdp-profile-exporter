package com.romash.repository.exception;

public class ProfileNotFoundException extends Exception {

    private final String id;

    public ProfileNotFoundException(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}
