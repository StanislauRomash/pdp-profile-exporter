package com.romash.service;

import com.romash.domain.Profile;
import com.romash.service.exception.ConvertionException;

import java.util.List;

public interface FileConvertService {

    List<Profile> convert(String filename) throws ConvertionException;
}
