package com.romash.service;

import com.mongodb.client.FindIterable;
import com.romash.domain.Profile;
import com.romash.repository.Repository;
import com.romash.util.ProfileConverter;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("mongoService")
public class MongoDBRepositoryService implements RepositoryService{

    private final Repository mongoRepository;

    private final ProfileConverter converter;

    @Autowired
    public MongoDBRepositoryService(@Qualifier("mongo") Repository mongoRepository,
                                    @Qualifier("mongoConverter") ProfileConverter converter) {
        this.mongoRepository = mongoRepository;
        this.converter = converter;
    }
    @Override
    public void saveAll(List<Profile> profiles) {
        mongoRepository.saveAll(converter.convertToDocuments(profiles));
    }

    @Override
    public List<Profile> findAll() {
        FindIterable<Document> documentIterable = mongoRepository.findAll();
        List<Document> documentList = documentIterable.into(new ArrayList<>());
        return converter.convertToProfiles(documentList);
    }

    @Override
    public Profile findById(String id) {
        return null;
    }

    @Override
    public Profile updateById(Profile profile, String id) {
        return null;
    }

    @Override
    public void saveOne(Profile profile, String id) {

    }

    @Override
    public void deleteOne(String id) {

    }
}
