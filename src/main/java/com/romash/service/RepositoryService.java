package com.romash.service;

import com.romash.domain.Profile;
import com.romash.repository.exception.ProfileNotFoundException;

import java.io.IOException;
import java.util.List;

public interface RepositoryService {
    void saveAll(List<Profile> profiles);

    List<Profile> findAll();

    Profile findById(String id) throws IOException, ProfileNotFoundException;

    Profile updateById(Profile profile, String id) throws IOException;

    void saveOne(Profile profile, String id) throws IOException;

    void deleteOne(String id) throws IOException;
}
