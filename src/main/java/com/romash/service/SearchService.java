package com.romash.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.romash.domain.Fields;
import com.romash.domain.Profile;
import com.romash.repository.ElasticsearchRepository;
import com.romash.request.FieldExclusionsParams;
import com.romash.request.RangedDateParams;
import com.romash.request.SortParams;
import org.elasticsearch.action.search.SearchResponseSections;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Qualifier("elasticsearchService")
public class SearchService {

    private ObjectMapper objectMapper;

    private ElasticsearchRepository repository;

    @Autowired
    public SearchService(
            @Qualifier("json") ObjectMapper objectMapper,
            ElasticsearchRepository repository) {
        this.objectMapper = objectMapper;
        this.repository = repository;
    }

    public Map<String, Long> aggregateWithinRange(String aggField, String rangeField,
                                       String rangeFrom, String rangeTo, String dateFormat) throws IOException {
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(rangeField)
                .from(rangeFrom, true)
                .to(rangeTo, true)
                .format(dateFormat);

        String aggregationName = aggField + "_agg";
        TermsAggregationBuilder termsAggregationBuilder =
                AggregationBuilders.terms(aggregationName)
                .field(aggField);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(rangeQueryBuilder)
                .aggregation(termsAggregationBuilder)
                .size(0);

        SearchResponseSections response = repository.search(searchSourceBuilder);

        Terms terms = response.aggregations().get(aggregationName);
        return terms.getBuckets().stream()
                .collect(Collectors.toMap(
                        Terms.Bucket::getKeyAsString,
                        Terms.Bucket::getDocCount));
    }

    public List<Profile> findByName(String name) throws IOException {
        MultiMatchQueryBuilder builder = QueryBuilders.multiMatchQuery(name,
                Fields.PROFILE_NAME, Fields.PROFILE_NAME_NGRAM)
                .fuzziness(1)
                .fuzzyTranspositions(true);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(builder);

        SearchResponseSections response = repository.search(searchSourceBuilder);
        return convertResponseToProfiles(response);
    }

    public List<Profile> searchInRange(RangedDateParams rangedFieldParam,
                                       FieldExclusionsParams fieldExclusionsParams,
                                       SortParams sortParams,
                                       int size) throws IOException {
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(rangedFieldParam.getDateField())
                .from(rangedFieldParam.getDateFrom(), true)
                .to(rangedFieldParam.getDateTo(), true)
                .format(rangedFieldParam.getDateFormat());

        TermsQueryBuilder termsQueryBuilder = QueryBuilders.termsQuery(
                fieldExclusionsParams.getExclusionField(),
                fieldExclusionsParams.getExcludedValues());

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery()
                .filter(rangeQueryBuilder)
                .mustNot(termsQueryBuilder);

        FieldSortBuilder fieldSortBuilder =  SortBuilders.fieldSort(sortParams.getSortBy())
                .order(SortOrder.fromString(sortParams.getOrder()));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(boolQueryBuilder)
                .sort(fieldSortBuilder)
                .size(size);

        SearchResponseSections response = repository.search(searchSourceBuilder);
        return convertResponseToProfiles(response);
    }

    private List<Profile> convertResponseToProfiles(SearchResponseSections response) throws JsonProcessingException {
        SearchHit[] searchHits = response.hits().getHits();
        List<Profile> profiles = new ArrayList<>(searchHits.length);
        for (SearchHit hit : searchHits) {
            profiles.add(objectMapper.readValue(hit.getSourceAsString(), Profile.class));
        }
        return  profiles;
    }

}
