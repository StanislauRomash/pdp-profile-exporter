package com.romash.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.romash.domain.Profile;
import com.romash.service.exception.ConvertionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service("json")
public class JsonFileConvertService implements FileConvertService {

    ObjectMapper objectMapper;

    @Autowired
    public JsonFileConvertService(@Qualifier("json")ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public List<Profile> convert(String filename) throws ConvertionException {
        List<Profile> profiles;
        try {
            profiles = objectMapper.readValue(new File(filename),
                                              new TypeReference<ArrayList<Profile>>() {});

            return Objects.nonNull(profiles) ? profiles
                                             : Collections.emptyList();
        } catch (IOException | NumberFormatException e) {
            throw new ConvertionException(filename, e);
        }
    }
}
