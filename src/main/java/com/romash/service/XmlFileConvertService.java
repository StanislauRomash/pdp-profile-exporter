package com.romash.service;

import com.romash.domain.Profile;
import com.romash.service.exception.ConvertionException;
import com.romash.util.ProfileSetters;
import com.romash.util.XmlFields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;

@Service("xml")
public class XmlFileConvertService implements FileConvertService {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlFileConvertService.class);

    @Autowired
    private ProfileSetters profileSetters;

    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    private XMLEventReader reader;

    @Override
    public List<Profile> convert(String filename) throws ConvertionException {
        List<Profile> profiles = new ArrayList<>();
        Profile profile = new Profile();
        try {
            reader = xmlInputFactory.createXMLEventReader(new FileInputStream(filename));
            while (reader.hasNext()) {
                XMLEvent nextEvent = reader.nextEvent();
                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    String fieldName = startElement.getName().getLocalPart();
                    if (fieldName.equals(XmlFields.PROFILE)) {
                        profile = new Profile();
                    } else {
                        setField(profile, fieldName, reader);
                    }
                }
                if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals(XmlFields.PROFILE)) {
                        profile.setExportDateTime(LocalDateTime.now());
                        profiles.add(profile);
                    }
                }
            }
            return profiles;
        } catch (XMLStreamException | FileNotFoundException e) {
            throw new ConvertionException(filename, e);
        } finally {
            if (Objects.nonNull(reader)) {
                try {
                    reader.close();
                } catch (XMLStreamException e) {
                    LOGGER.error("Unable to close XMLEventReader for file: " + filename);
                }
            }
        }
    }

    private void setField(Profile profile, String fieldName, XMLEventReader reader) throws XMLStreamException {
        XMLEvent nextEvent = reader.nextEvent();
        String fieldValue = nextEvent.asCharacters().getData();
        if (profileSetters.contains(fieldName)) {
            BiConsumer<Profile, String> setter = profileSetters.get(fieldName);
            setter.accept(profile, fieldValue);
        } else {
            profile.setAdditionalField(fieldName, fieldValue);
        }
    }

}
