package com.romash.service.exception;

public class ConvertionException extends Exception {

    public ConvertionException(String filename, Throwable cause) {
        super("Error while converting file: " + filename, cause);
    }

}
