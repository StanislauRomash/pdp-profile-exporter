package com.romash;


import com.romash.domain.Profile;
import com.romash.service.FileConvertService;
import com.romash.service.RepositoryService;
import com.romash.service.exception.ConvertionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@Qualifier("profileConsoleController")
public class ProfileConsoleController {

    private final Map<String, FileConvertService> fileConvertServices;
    private final RepositoryService repositoryService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileConsoleController.class);

    public ProfileConsoleController(@Qualifier("json") FileConvertService jsonFileConvertService,
                                    @Qualifier("xml") FileConvertService xmlFileConvertService,
                                    @Qualifier("mongoService") RepositoryService repositoryService) {
        this.repositoryService = repositoryService;

        fileConvertServices = new HashMap<>();
        fileConvertServices.put(".json", jsonFileConvertService);
        fileConvertServices.put(".xml", xmlFileConvertService);
    }


    public void importFile(String filename) {
        LOGGER.info("Start importing file: " + filename);
        // find file convert service by file extension
        String fileExtension = getFileExtension(filename);
        FileConvertService fileConvertService = fileConvertServices.get(fileExtension);
        if (Objects.isNull(fileConvertService)) {
            String message = "File '" + filename + "' is not supported!\n" +
                    "Supported formats: .json, .xml";
            LOGGER.warn(message);
            // write about incorrect file format to console
            // because it's console app
            System.err.println(message);
            return;
        }

        List<Profile> profiles;
        try {
            profiles = fileConvertService.convert(filename);
            LOGGER.info("File " + filename + " has read successfully.");

            repositoryService.saveAll(profiles);
            LOGGER.info(profiles.size() + " documents have been added to database successfully.");
        } catch (ConvertionException e) {
            LOGGER.error("Error during import file: " + filename, e);
        }
    }

    public Collection<Profile> showAll() {
        LOGGER.info("Start finding profiles documents in database...");
        Collection<Profile> profiles = repositoryService.findAll();
        LOGGER.info(profiles.size() + " documents are found:");
        profiles.forEach(System.out::println);
        LOGGER.info("All documents are shown.");
        return profiles;
    }

    private String getFileExtension(String filename) {
        return filename.replaceAll("^.*(?=[.].*$)", "");
    }

}
