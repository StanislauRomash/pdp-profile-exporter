package com.romash.rest;


import com.romash.repository.exception.ProfileNotFoundException;
import org.elasticsearch.ElasticsearchStatusException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class ProfileExceptionsHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { ProfileNotFoundException.class })
    protected ResponseEntity<Object> handleProfileNotFound(
            ProfileNotFoundException ex, WebRequest request) {
        String bodyOfResponse = String.format("Profile with id = '%s' is not found", ex.getId());
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = { IOException.class })
    protected ResponseEntity<Object> handleIOException(
            IOException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
    @ExceptionHandler(value = { ElasticsearchStatusException.class })
    protected ResponseEntity<Object> handleElasticSearchExceptions(
            ElasticsearchStatusException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
