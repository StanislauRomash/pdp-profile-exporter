package com.romash.rest;

import com.romash.domain.Profile;
import com.romash.service.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/mongodb/profiles")
@Qualifier("profileMongoRestController")
public class ProfileMongoDBRestController {

    private final RepositoryService repositoryService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileMongoDBRestController.class);

    public ProfileMongoDBRestController(@Qualifier("mongoService") RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveProfiles(@RequestBody List<Profile> profiles) {
        LOGGER.info("Start saving {} profile(s): ", profiles.size());
        // find file convert service by file extension
        repositoryService.saveAll(profiles);
        LOGGER.info("{} documents have been added to database successfully.", profiles.size());
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Profile> getAllProfiles(@RequestParam(required = false) String dateFormat) {
        LOGGER.info("Start finding profiles documents in database...");
        List<Profile> profiles = repositoryService.findAll();
        LOGGER.info("{} documents are found", profiles.size());
        return profiles;
    }

}
