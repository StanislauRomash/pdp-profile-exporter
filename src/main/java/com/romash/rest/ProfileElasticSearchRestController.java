package com.romash.rest;

import com.romash.domain.Fields;
import com.romash.domain.Profile;
import com.romash.repository.exception.ProfileNotFoundException;
import com.romash.request.FieldExclusionsParams;
import com.romash.request.RangedDateParams;
import com.romash.request.SortParams;
import com.romash.service.RepositoryService;
import com.romash.service.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/es/profiles")
@Qualifier("profileElasticSearchRestController")
public class ProfileElasticSearchRestController {

    private final RepositoryService repositoryService;

    private final SearchService searchService;

    private static final Logger LOGGER = LoggerFactory.getLogger(
            ProfileElasticSearchRestController.class);

    public ProfileElasticSearchRestController(
            @Qualifier("elasticsearchService") RepositoryService repositoryService,
            @Qualifier("elasticsearchService") SearchService searchService) {
        this.repositoryService = repositoryService;
        this.searchService = searchService;
    }

    @PostMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveProfile(@RequestBody Profile profile,
                            @PathVariable String id) throws IOException {
        LOGGER.info("Start saving profile with id: {} ", id);
        // find file convert service by file extension
        repositoryService.saveOne(profile, id);
        LOGGER.info("Document have been added to Elasticsearch successfully.");
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Profile getProfile(@PathVariable String id) throws IOException, ProfileNotFoundException {
        LOGGER.info("Start finding a profile document in database...");
        Profile profile = repositoryService.findById(id);
        LOGGER.info("Document is found");
        return profile;
    }

    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Profile updateProfile(@RequestBody Profile profile,
                            @PathVariable String id) throws IOException {
        LOGGER.info("Start updating profile with id: {} ", id);
        // find file convert service by file extension
        Profile updatedProfile = repositoryService.updateById(profile, id);
        LOGGER.info("Document have been updated in Elasticsearch successfully.");
        return updatedProfile;
    }

    @DeleteMapping(path = "/{id}")
    public void deleteProfile(@PathVariable String id) throws IOException {
        LOGGER.info("Start updating profile with id: {} ", id);
        // find file convert service by file extension
        repositoryService.deleteOne(id);
        LOGGER.info("Document have been deleted in Elasticsearch successfully.");
    }


    /**
     * Endpoint returns Profiles within specified date range
     * <p>
     * From returned profiles can be excluded from response. List of Profiles can be sorted.
     * <p>
     * Examples:
     * http://localhost:8080/api/es/profiles
     * http://localhost:8080/api/es/profiles?dateFrom=2021-02-01&dateTo=2021-04-01&excludeType=PRIVATE
     * http://localhost:8080/api/es/profiles?dateFrom=2021/02/01&dateTo=2021/05/01&dateFormat=yyyy/MM/dd&excludedTypes=PRIVATE,PUBLIC&sortBy=registerDate&order=DESC
     *
     * @param rangedDateParams accept dateField (defaul - registerDate), dateFrom, dateTo,
     *                         dateFormat (default - "yyyy-MM-dd") to limit Profiles in response by date range
     * @param excludedTypes list of Profile types to exclude from results
     * @param sortParams sorting parameters (sortBy and order) to sort by Profile field,
     *                   default values - sortBy = registerDate, order = ASC.
     * @return JSON represented aggregated types with its document counts
     * @throws IOException throws exception if something wrong (todo - implement custom exception)
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Profile> getProfiles(RangedDateParams rangedDateParams,
                                     @RequestParam(required = false) String dateFormat,
                                     @RequestParam(required = false) List<String> excludedTypes,
                                     SortParams sortParams,
                                     @RequestParam(required = false, defaultValue = "10") int size)
            throws IOException {
        LOGGER.info("Start finding a profiles in database...");
        FieldExclusionsParams fieldExclusionsParams =
                new FieldExclusionsParams(Fields.PROFILE_TYPE, excludedTypes);

        List<Profile> profiles = searchService.searchInRange(rangedDateParams,
                fieldExclusionsParams, sortParams, size);
        LOGGER.info("Profiles is found");
        return profiles;
    }


    /**
     * Endpoint returns aggregations by ProfileType within specified date range
     * <p>
     * Aggregated Profiles can restricted by date range
     * <p>
     * Examples:
     * http://localhost:8080/api/es/profiles/types
     * http://localhost:8080/api/es/profiles/types?dateFrom=2021-02-01&dateTo=2021-04-01
     * http://localhost:8080/api/es/profiles/types?dateFrom=2021/02/01&dateTo=2021/04/01&dateFormat=yyyy/MM/dd
     *
     *
     * @param dateFrom limit Profiles to aggregate registered since this date
     * @param dateTo limit Profiles to aggregate registered until this date
     * @param dateFormat custom date format instead of default "yyyy-MM-dd"
     * @return JSON represented aggregated types with its document counts
     * @throws IOException throws exception if something wrong (todo - implement custom exception)
     */
    @GetMapping(path = "/types", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Long> getProfileTypes(@RequestParam(required = false) String dateFrom,
                                  @RequestParam(required = false) String dateTo,
                                  @RequestParam(required = false, defaultValue = "yyyy-MM-dd") String dateFormat)
            throws IOException {
        LOGGER.info("Start finding a profiles types in database...");
        Map<String, Long> aggregations = searchService.aggregateWithinRange(Fields.PROFILE_TYPE,
                Fields.REGISTER_DATE, dateFrom, dateTo, dateFormat);
        LOGGER.info("Profiles types is found");
        return aggregations;
    }

    /**
     * Endpoint for searching profiles by profile name.
     * Queried name should contain at least 3 symbols, can contain 1 mistake.
     * Search is case-insensitive.
     *
     * <p>
     * Examples:
     * http://localhost:8080/api/es/profiles/findProfiles?name=Mlik
     *
     * @param name name or part of name to search profiles by profile name
     * @return list of found profiles
     * @throws IOException throws exception if something wrong (todo - implement custom exception)
     */
    @GetMapping(path = "/findProfiles", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Profile> findProfilesByName(@RequestParam String name)
            throws IOException {
        LOGGER.info("Start finding a profiles by name...");
        List<Profile> profiles = searchService.findByName(name);
        LOGGER.info("Profiles by name is found");
        return profiles;
    }
}
