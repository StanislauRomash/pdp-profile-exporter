package com.romash.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;

@Component
public class LocalDateSerializer extends JsonSerializer<LocalDate> {

    @Override
    public void serialize(LocalDate value,
                          JsonGenerator gen,
                          SerializerProvider serializers) throws IOException {
        gen.writeString(String.valueOf(
                value.atStartOfDay(ZoneOffset.UTC)
                        .toInstant()
                        .toEpochMilli()));
    }
}
