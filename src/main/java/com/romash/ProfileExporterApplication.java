package com.romash;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;

public class ProfileExporterApplication {

    // list of available commands
    private static final String IMPORT = "import";
    private static final String SHOW_ALL = "showAll";

    private static Map<String, BiConsumer<ProfileConsoleController, String[]>> commands;

    private static Map<String, Integer> commandArgs;

    static {
        commands = new HashMap<>();
        commands.put(IMPORT, (profileConsoleController, args) -> profileConsoleController.importFile(args[1]));
        commands.put(SHOW_ALL, (profileConsoleController, args) -> profileConsoleController.showAll());

        commandArgs = new HashMap<>();
        // command name -> # of args from command line
        commandArgs.put(IMPORT, 2);
        commandArgs.put(SHOW_ALL, 1);
    }

    public static void main(String[] args) {
        if (!isCorrectArgs(args)) {
            printCommandUsage();
            return;
        }
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "profile-exporter-console-root-context.xml");
        ProfileConsoleController profileConsoleController = context.getBean(
                "profileConsoleController", ProfileConsoleController.class);
        // get command and execute it
        commands.get(args[0]).accept(profileConsoleController, args);

    }

    private static boolean isCorrectArgs(String[] args) {
        if (Objects.isNull(args) || args.length == 0) {
            return false;
        }
        if (!commandArgs.containsKey(args[0])) {
            return false;
        }
        if (commandArgs.get(args[0]) != args.length) {
            return false;
        }
        return true;
    }

    private static void printCommandUsage() {
        System.out.println("Incorrect command.\n" +
                "Usage: <command> <arg>\n" +
                "Commands:\n" +
                "   import <filename>   - imports file with profiles into database\n" +
                "   showAll             - shows all profiles from database");
    }
}
