package com.romash.domain;

public enum ProfileType {
    PRIVATE,
    PROTECTED,
    PUBLIC
}
