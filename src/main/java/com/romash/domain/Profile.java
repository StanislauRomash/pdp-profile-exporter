package com.romash.domain;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.romash.serialization.LocalDateDeserializer;
import com.romash.serialization.LocalDateTimeDeserializer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Profile {

    @JsonProperty("profileName")
    private String profileName;

    @JsonProperty("registerDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate registerDate;

    @JacksonInject("exportDateTime")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime exportDateTime;

    private Map<String, Object> additionalFields = new HashMap<>();

    @JsonProperty("profileType")
    private ProfileType profileType;


    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }

    public LocalDateTime getExportDateTime() {
        return exportDateTime;
    }

    public void setExportDateTime(LocalDateTime exportDateTime) {
        this.exportDateTime = exportDateTime;
    }

    @JsonAnySetter
    public void setAdditionalField(String fieldName, String fieldValue) {
        additionalFields.put(fieldName, fieldValue);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalFields() {
        return additionalFields;
    }

    public ProfileType getProfileType() {
        return profileType;
    }

    public void setProfileType(ProfileType profileType) {
        this.profileType = profileType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return profileName.equals(profile.profileName)
                && registerDate.equals(profile.registerDate)
                && additionalFields.equals(profile.additionalFields);
    }

    @Override
    public int hashCode() {
        return Objects.hash(profileName, registerDate, exportDateTime, additionalFields);
    }

    @Override
    public String toString() {
        return "Profile{" +
                "profileName='" + profileName + '\'' +
                ", registerDate='" + registerDate + '\'' +
                ", profileType='" + profileType + '\'' +
                ", exportDateTime=" + exportDateTime +
                ", fields=" + additionalFields +
                '}';
    }
}
