package com.romash.domain;

public abstract class Fields {

    private Fields() {}

    public static final String PROFILE_NAME = "profileName";
    public static final String PROFILE_NAME_NGRAM = "profileName.ngram";
    public static final String PROFILE_TYPE = "profileType";
    public static final String REGISTER_DATE = "registerDate";
    public static final String EXPORT_DATE_TIME = "exportDateTime";
    public static final String DESCRIPTION = "description";
}
