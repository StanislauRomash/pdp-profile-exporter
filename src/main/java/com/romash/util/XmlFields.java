package com.romash.util;

public class XmlFields {

    public static final String PROFILE = "profile";
    public static final String PROFILE_NAME = "profileName";
    public static final String REGISTER_DATE = "registerDate";
    public static final String PROFILE_TYPE = "profileType";

    private XmlFields() {
        // private constructor for helper class
    }

}
