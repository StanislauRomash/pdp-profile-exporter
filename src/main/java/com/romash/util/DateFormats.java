package com.romash.util;

public enum DateFormats {
    YMD("yyyy-MM-dd"),
    DMY("dd/MM/yyyy"),
    MDY("MM/dd/yyyy");

    private String format;

    DateFormats(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }
}
