package com.romash.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Component
public class DateTransformer {

    private DateTimeFormatter formatter;

    public DateTransformer(@Value("${date.format}") DateFormats dateFormat) {
        formatter = DateTimeFormatter.ofPattern(dateFormat.getFormat())
                .withZone(ZoneId.systemDefault());
    }

    public String transformMillisToStringDate(String registerDateMillis) {
        Instant instant = Instant.ofEpochMilli(Long.parseLong(registerDateMillis));
        return formatter.format(instant);
    }
}
