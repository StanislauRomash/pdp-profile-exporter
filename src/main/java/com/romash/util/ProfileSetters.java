package com.romash.util;

import com.romash.domain.Profile;
import com.romash.domain.ProfileType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

@Component
public class ProfileSetters {

    // map with lambdas to set required fields
    private Map<String, BiConsumer<Profile, String>> requiredFields;
    private final BiConsumer<Profile, String> profileNameSetter = Profile::setProfileName;

    private final BiConsumer<Profile, String> registerDateSetter = (profile, registerDateMillis) ->
            profile.setRegisterDate(Instant.ofEpochMilli(Long.parseLong(registerDateMillis))
                    .atOffset(ZoneOffset.UTC)
                    .toLocalDate());

    private final BiConsumer<Profile, String> profileTypeSetter = (profile, profileTypeString) ->
            profile.setProfileType(ProfileType.valueOf(profileTypeString));

    @Autowired
    public ProfileSetters() {
        requiredFields = new HashMap<>();
        requiredFields.put(XmlFields.PROFILE_NAME, profileNameSetter);
        requiredFields.put(XmlFields.REGISTER_DATE, registerDateSetter);
        requiredFields.put(XmlFields.PROFILE_TYPE, profileTypeSetter);
    }

    public boolean contains(String key) {
        return requiredFields.containsKey(key);
    }

    public BiConsumer<Profile, String> get(String key) {
        return requiredFields.get(key);
    }
}
