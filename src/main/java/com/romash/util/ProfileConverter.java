package com.romash.util;

import com.romash.domain.Profile;
import com.romash.domain.ProfileType;
import com.romash.repository.DBFields;
import org.bson.Document;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

@Component("mongoConverter")
public class ProfileConverter {

    // map with lambdas to set required fields
    private Map<String, BiConsumer<Profile, Object>> profileSetters;

    private final BiConsumer<Profile, Object> profileNameSetter = (profile, object) ->
            profile.setProfileName((String) object);
    private final BiConsumer<Profile, Object> registerDateSetter = (profile, object) ->
            profile.setRegisterDate(Instant.ofEpochMilli((long) object)
                    .atOffset(ZoneOffset.UTC)
                    .toLocalDate());
    private final BiConsumer<Profile, Object> exportDateTimeSetter = (profile, object) ->
            profile.setExportDateTime(Instant.ofEpochMilli((long) object)
                    .atOffset(ZoneOffset.UTC)
                    .toLocalDateTime());
    private final BiConsumer<Profile, Object> profileTypeSetter = (profile, object) ->
            profile.setProfileType(ProfileType.valueOf((String) object));

    public ProfileConverter() {
        profileSetters = new HashMap<>();
        profileSetters.put(DBFields.PROFILE_NAME, profileNameSetter);
        profileSetters.put(DBFields.REGISTER_DATE, registerDateSetter);
        profileSetters.put(DBFields.EXPORT_DATE_TIME, exportDateTimeSetter);
        profileSetters.put(DBFields.PROFILE_TYPE, profileTypeSetter);
    }

    public Document convertToDocument(Profile profile) {
        Document document = new Document(DBFields.PROFILE_NAME, profile.getProfileName())
                .append(DBFields.REGISTER_DATE,
                        profile.getRegisterDate()
                                .atStartOfDay()
                                .toInstant(ZoneOffset.UTC)
                                .toEpochMilli())
                .append(DBFields.EXPORT_DATE_TIME,
                        profile.getExportDateTime()
                                .toInstant(ZoneOffset.UTC)
                                .toEpochMilli())
                .append(DBFields.PROFILE_TYPE,
                        profile.getProfileType()
                                .toString());
        profile.getAdditionalFields().forEach(document::append);
        return document;
    }

    public List<Document> convertToDocuments(List<Profile> profiles) {
        List<Document> documents = new ArrayList<>(profiles.size());
        profiles.forEach(profile -> documents.add(convertToDocument(profile)));
        return documents;
    }

    public Profile convertToProfile(Document document) {
        Profile profile = new Profile();
        for(Map.Entry<String, Object> field : document.entrySet()) {
            if (profileSetters.containsKey(field.getKey())) {
                BiConsumer<Profile, Object> setter = profileSetters.get(field.getKey());
                setter.accept(profile, field.getValue());
            } else {
                profile.setAdditionalField(field.getKey(), field.getValue().toString());
            }
        }
        return profile;
    }

    public List<Profile> convertToProfiles(List<Document> documents) {
        List<Profile> profiles = new ArrayList<>(documents.size());
        documents.forEach(document -> profiles.add(convertToProfile(document)));
        return profiles;
    }
}
