GET localhost:9200/profiles/_search

{
    "query": {
        "range": {
            "registerDate": {
                "gte": "2021-02-01",
                "lt": "2021-03-01",
                "format": "strict_date"
            }
        }
    },
    "aggs": {
        "profile_types": {
            "terms": {
                "field": "profileType"
            }
        }
    }
}